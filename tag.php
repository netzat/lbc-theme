<?php /**** Only in ladbrokes ****/ ?>
<?php get_header(null, ['class' => 'category']); ?>

<section>
    <h1 class="category-title"><?php single_tag_title() ?></h1>
    <div class="category-description"><?= category_description() ?></div>
    <?php get_template_part('loop') ?>
    <?php get_template_part('pagination') ?>
</section>

<?php get_footer();?>
