<?php while (have_posts()): the_post(); ?>
    <article id="post-<?= get_the_ID() ?>" <?php post_class('loop-item') ?>>
        <div class="cat-item-img">
            <?php if (hasThumbnail()): ?>
                <a href="<?= get_permalink() ?>" title="<?= get_the_title() ?>">
                    <?php imgs(get_the_ID(), ['desk' => '315x132', 'tab' => '355x204', 'mobi' => '575x219'], 'medium_large', isLadbrokes()) ?>
                </a>
            <?php endif; ?>
        </div>

        <div class="cat-item-details">
            <h2>
                <a href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>"><?= get_the_title() ?></a>
            </h2>
            <div class="cat-item-dateauthor">
                <span class="cat-item-author"><?= get_the_author_posts_link() ?></span> |
                <span class="cat-item-date"><?= get_the_time('d.m.Y') ?></span>
            </div>
            <p class="cat-item-desc"><?= shortenText(get_the_excerpt(), 400, '... ') ?></p>
        </div>
    </article>
<?php endwhile; ?>
