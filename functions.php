<?php

add_action('wp_enqueue_scripts', function () {
    wp_dequeue_style('html5blank');
    wp_deregister_style('html5blank');
}, 25);

add_action('wp_enqueue_scripts', function () {
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        $registerStyle = function($name, $path) {
            wp_register_style($name,  get_stylesheet_directory_uri().$path);
            wp_enqueue_style($name);
        };
        $registerStyle('fontawesome',      '/css/fontawesome/css/all.min.css');
        $registerStyle('html5blank-child', '/css/'.getClientDir().'/style.css');
        $registerStyle('responsive',       '/css/'.getClientDir().'/responsive.css');
    }
});

add_action('init', function () {
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'rest_output_link_wp_head', 10);
    remove_action('wp_head', 'wp_resource_hints', 2);
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('admin_print_styles', 'print_emoji_styles');
    add_action('admin_menu', fn () => add_menu_page('SiteSettings', 'Site Settings', 'edit_pages', admin_url('post.php?post='.get_page_by_path('settings')->ID.'&action=edit'), '', 'dashicons-admin-page', 75));
    add_filter('excerpt_length', fn ($length) => isCoral() ? 50 : 80, 999);
    add_filter('the_generator', function() { return ''; });
    remove_action('template_redirect', 'redirect_canonical');
    register_nav_menu('footer_menu',__('Footer menu'));
    if (isCoral()) {
        add_filter('admin_post_thumbnail_html', fn ($html) => $html.'<p>Ideal size: <strong>1280px x 768px</strong></p>');
    } else {
        add_filter('nav_menu_css_class' , function($classes, $item) {
            if (in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes)){
                $classes[] = 'cur-link-active';
            }
            return $classes;
        }, 10 , 2);
    }
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
        wp_register_script('main', get_stylesheet_directory_uri() . '/js/'.getClientDir().'/main.js', '', '', true);
        wp_enqueue_script('main');
    }
});

/**** Only in coral ****/
function getConfig(string $key, ?string $default = null)/*: mixed */
{
    static $config;
    $config = $config ?: get_post_meta(get_page_by_path('settings')->ID);
    return $config[$key][0] ?? $default;
}

/**** Only in ladbrokes ****/
function getSpecConfig(): array
{
    static $config;
    return $config ?: get_fields(308836);
}

function shortenText(string $str, int $len, bool $hp = false): string
{
    $str = strip_tags($str);

    if (mb_strlen($str) > $len) {
        return mb_substr($str, 0, $len).'... '.($hp ? ' <a href="'.get_permalink().'">more...</a>' : '');
    }

    return $str;
}

function hasThumbnail(?int $id = null): ?bool
{
    $url = get_the_post_thumbnail_url($id);

    return $url ? is_file(getcwd().parse_url($url)['path']) : false;
}

function ___(string $string) : string
{
    return __($string, 'html5blank');
}

function img(string $key): string
{
    return wp_get_attachment_image_src($key, 'full')[0];
}

function imgs(int $id, array $imgs, string $thumb, ?bool $skipSearch = false): void
{
    if($skipSearch) {
        the_post_thumbnail($thumb);
        return;
    }

    $show = false;
    $meta = get_post_meta($id);

    foreach($imgs as $class => $img) {
        $imgs[$class] = sprintf('featured_image_%s_px', $img);
    }

    foreach($imgs as $img) {
        if($meta[$img][0] ?? null) {
            $show = true;
            break;
        }
    }

    if($show) {
        foreach($imgs as $class => $img) {
            if($src = img($meta[$img][0])) {
                printf('<img class="img-%s" src="%s"/>', $class, $src);
            }
        }
    } else {
        the_post_thumbnail($thumb);
    }
}

function isCoral(): bool
{
    static $isCoral = null;

    if(!is_null($isCoral)) {
        return $isCoral;
    }

    $coral   = ['sports.coral.co.uk', 'coral.ninjas.at'];
    $host    = parse_url(get_site_url(null, null, null))['host'];
    $isCoral = in_array($host, $coral);

    return $isCoral;
}

function isLadbrokes(): bool
{
    return !isCoral();
}

function getClientDir(): string
{
    return isCoral() ? '_coral' : '_ladbrokes';
}
