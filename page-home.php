<?php

get_header();

$params = [
    'post_type'           => 'post',
    'post_status'         => 'publish',
    'orderby'             => 'date',
    'order'               => 'DESC',
    'ignore_sticky_posts' => 1,
];

$query1 = new WP_Query($params + ['posts_per_page' => 1]);
$query2 = new WP_Query($params + ['posts_per_page' => 3, 'offset' => 1]);
$query3 = new WP_Query($params + ['posts_per_page' => 6]);

?>

<section class="home-banner">
    <?php while ($query1->have_posts()): $query1->the_post(); ?>
        <div class="main-img">
            <a href="<?= get_permalink() ?>">
                <?php imgs(get_the_ID(), ['desk' => '742x420', 'mobi' => '575x219'], 'large', isLadbrokes()) ?>
                <div class="hero-text">
                    <p><?= get_the_title() ?></p>
                </div>
            </a>
        </div>
    <?php endwhile; ?>

    <div class="most-read-list">
        <?php while ($query2->have_posts()): $query2->the_post(); ?>
            <div class="most-read-imgs">
                <a href="<?= get_permalink() ?>">
                    <?php imgs(get_the_ID(), ['desk' => '315x132', 'tab' => '245x245', 'mobi' => '575x219'], 'medium_large', isLadbrokes()) ?>
                    <div class="most-read-imgs-title">
                        <p><?= get_the_title() ?></p>
                    </div>
                </a>
            </div>
        <?php endwhile; ?>
    </div>
</section>

<section class="home-content">
    <h3 class="news-section-title"><?= isCoral() ? getConfig('news_label') : 'SPORTS NEWS' ?></h3>
    <div class="news-wrapper">
        <?php while ($query3->have_posts()): $query3->the_post(); ?>
            <div class="news-item">
                <div class="news-img">
                    <a href="<?= get_permalink() ?>">
                        <?php imgs(get_the_ID(), ['desk' => '355x204', 'mobi' => '575x219'], 'medium_large', isLadbrokes()) ?>
                    </a>
                </div>
                <div class="news-text">
                    <div class="news-title"><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></div>
                    <p class="news-desc"><?= shortenText(get_the_excerpt(), 150, true) ?></p>
                </div>
                <div class="news-btn">
                    <a href="<?= get_permalink() ?>"><span><?= isCoral() ? getConfig('news_post_button_label') : 'Read more' ?></span></a>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</section>

<?php get_footer() ?>
