<?php get_header() ?>

<section>
    <h1 class="search-title"><?= $wp_query->found_posts.___(' Search Results for ').get_search_query() ?></h1>
    <?php get_template_part('loop') ?>
    <?php get_template_part('pagination') ?>
</section>

<?php get_footer() ?>
