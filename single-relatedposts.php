<?php

$query = new WP_Query([
    'post_type'           => 'post',
    'post_status'         => 'publish',
    'post__not_in'        => [get_the_ID()],
    'posts_per_page'      => 3,
    'orderby'             => 'rand',
    'ignore_sticky_posts' => 1,
    'tax_query' => [
        'relation'  => 'OR',
        ['taxonomy' => 'category', 'field' => 'term_id', 'terms' => array_map(fn ($cat) => $cat->term_id, get_the_category($post->ID))],
        ['taxonomy' => 'post_tag', 'field' => 'term_id', 'terms' => array_map(fn ($tag) => $tag->term_id, wp_get_post_tags($post->ID))]
    ]
]);

?>

<?php if ($query->have_posts()): ?>
    <div class="related-posts-wrapper">
        <div class="related-posts-header">Related Posts</div>
        <div class="related-posts-list">
        <?php while ($query->have_posts()):  $query->the_post(); ?>
            <div class="related-posts-item">
                <div class="related-posts-img">
                    <a href="<?= get_permalink() ?>">
                        <?php imgs(get_the_ID(), ['desk' => '355x204', 'mobi' => '575x219'], 'medium_large', isLadbrokes()) ?>
                    </a>
                </div>
                <div class="related-posts-text">
                    <p class="related-posts-details">
                        <?php if ($author = get_the_author_posts_link()): ?>
                            <span class="rpd-author"><?= $author ?></span> |
                        <?php endif; ?>
                        <span class="rpd-date"><?= get_the_time('d.m.Y') ?></span>
                    </p>
                    <div class="related-posts-title"><a href="<?= get_permalink() ?>"><?= get_the_title() ?></a></div>
                </div>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>
