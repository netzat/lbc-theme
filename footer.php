                </div>
            </main>

            <footer class="footer" role="contentinfo">
                <div class="footer-wrapper">

                    <?php if (isCoral()): ?>
                        <div class="footer-menu-title"><?= getConfig('footer_menu_title') ?></div>
                        <div class="footer-links"><?php wp_nav_menu(['theme_location' => 'footer_menu']) ?></div>
                        <div class="soc-med-wrapper">
                            <?php foreach (range(0, getConfig('social_media')) as $i): ?>
                                <?php if (getConfig('social_media_'.$i.'_label')): ?>
                                    <div class="soc-med-item">
                                        <a href="<?= getConfig('social_media_'.$i.'_link') ?>">
                                            <img class="soc-med-item-img" src="<?= img(getConfig('social_media_'.$i.'_icon')) ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="footer-about"><?= nl2br(getConfig('about')) ?></div>
                        <div class="assoc-wrapper">
                            <?php foreach (range(0, getConfig('assoc')) as $i): ?>
                                <?php if (getConfig('assoc_'.$i.'_icon')): ?>
                                    <div class="assoc-item">
                                        <a href="<?= maybe_unserialize(getConfig('assoc_'.$i.'_link'))['url'] ?>">
                                            <img class="assoc-item-img" src="<?= img(getConfig('assoc_'.$i.'_icon')) ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="footer-copyright"><?= getConfig('copyright_label') ?> &copy; <?= date('Y') ?> <?= getConfig('copyright_text') ?></div>
                        <div class="footer-text"><?= nl2br(getConfig('description')) ?></div>

                    <?php else: ?>

                        <div class="footer-adlinks">
                            <p><?= getSpecConfig()['footer_content']['footer_links_title'] ?></p>
                            <?php wp_nav_menu(['theme_location' => 'footer_menu']) ?>
                        </div>
                        <div class="soc-med-wrapper">
                            <?php
                                $sm = ['facebook', 'twitter', 'google_plus', 'youtube', 'instagram'];
                                $sm = array_combine($sm, $sm);
                                $sm['facebook'] = 'fb';
                            ?>
                            <?php foreach ($sm as $k => $v): ?>
                                <?php if ([$v.'_visibility'] ?? null): ?>
                                    <?php $conf = getSpecConfig()['footer_content']['footer_social_media'][$k.'_options'] ?>
                                    <div class="soc-med-item">
                                        <a href="<?= $conf[$v.'_page_link']['url']; ?>" target="<?= $conf[$v.'_page_link']['target'] ?>">
                                            <img class="soc-med-item-img" src="<?= $conf[$v.'_image_icon'] ?>">
                                        </a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                        <div class="footer-about">
                            <div class="footer-assoc-links"><?= getSpecConfig()['footer_content']['footer_first_paragraph']; ?></div>
                            <div class="footer-assocs">
                                <?php foreach(getSpecConfig()['footer_content']['footer_assoc'] as $item): ?>
                                    <div class="footer-assoc-data">
                                           <a href="<?= $item['url']; ?>" target="_blank">
                                             <img class="assoc-item-img" src="<?= $item['icon']; ?>">
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="footer-copyright"><?= getSpecConfig()['footer_content']['footer_copyright'] ?></div>
                        <div class="footer-text"><?= getSpecConfig()['footer_content']['footer_second_paragraph'] ?></div>

                    <?php endif; ?>
                </div>
            </footer>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>
