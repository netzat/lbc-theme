<?php /**** Only in coral ? ****/ ?>
<?php get_header(null, ['class' => 'post']) ?>

<?php if (isCoral()): ?>
    <section>
        <article id="post-<?= get_the_ID() ?>" <?php post_class() ?>>
            <div class="post-body">
                <?php if($title ?? null): ?>
                    <h1 class="post-title"><?= get_the_title() ?></h1>
                <?php endif; ?>
                <div class="post-content">
                    <div class="page-desc"><?php the_content() ?></div>
                </div>
            </div>
        </article>
    </section>
 <?php else: ?>
    <?php the_post()    ?>
    <?php the_content() ?>
 <?php endif; ?>

<?php get_footer() ?>
