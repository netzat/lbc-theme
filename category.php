<?php get_header(null, ['class' => 'category']) ?>

<section>
    <h1 class="category-title"><?php single_cat_title() ?></h1>
    <?php if ($term = term_description()): ?>
        <p class="category-subtitle"><?= $term ?></p>
    <?php endif; ?>
    <?php get_template_part('loop') ?>
    <?php get_template_part('pagination') ?>
</section>

<?php get_footer() ?>
