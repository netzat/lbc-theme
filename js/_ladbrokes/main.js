(function ($, root, undefined) {

    $(function () {

        'use strict';

        $('.mobile-menu').click(function(event){
            $("#mobile-menu").addClass('active');
            $('body').addClass('mobile-active');
        });

        $('.mobile-menu-close').click(function(event){
            $("#mobile-menu").removeClass('active');
            $('body').removeClass('mobile-active');
        });

        $(window).on('resize',function(e){
            if ($(this).width() > 1024) {
                $('#mobile-menu').removeClass('active');
            }
        });

        $('#mobile-menu > ul li.menu-item-has-children > a').after('<span><i class="fa fa-caret-down fa-active"></i><i class="fa fa-caret-up"></i></span>');

        $('#mobile-menu > ul li.menu-item-has-children > span').on('click',function(){
                $(this).siblings('.sub-menu').stop().slideToggle();
            $(this).children().toggleClass('fa-active');
        })

        $('.menu-more-sports').on('click',function(e){
            e.preventDefault();
        })
    });

})(jQuery, this);
