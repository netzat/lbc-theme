(function ($, root, undefined) {

    $(function () {
        'use strict';

        $('.mobile-menu').click(function(e){
            $('#mobile-menu').addClass('active');
            $('body').addClass('mobile-active');
        });

        $('.mobile-menu-close').click(function(e){
            $('#mobile-menu').removeClass('active');
            $('body').removeClass('mobile-active');
        });

        $('.desk-more-menu > ul > li.menu-item-has-children > a').each(function(e){
            $(this).after('<span><i class="fa fa-caret-down fa-active"></i><i class="fa fa-caret-up"></i></span>');
        });

        $('.desk-more-menu > ul > li.menu-item-has-children span').click(function(e){
            $(this).next().slideToggle('slow');
            $(this).children().toggleClass('fa-active');
        });

        $('#mobile-menu > ul > li.menu-item-has-children > a').each(function(e){
            $(this).after('<span><i class="fa fa-caret-down fa-active"></i><i class="fa fa-caret-up"></i></span>');
        });

        $('#mobile-menu > ul > li.menu-item-has-children span').click(function(e){
            $(this).next().slideToggle('slow');
            $(this).children().toggleClass('fa-active');
        });

        $(window).on('resize',function(e){
            if ($(this).width() > 1024) {
                $('#mobile-menu').removeClass('active');
                $('body').removeClass('mobile-active');
            }
        });

        $('.dropbtn').click(function(e){
            $(this).next().slideToggle('slow');
        });

        $('#avg90-mins-menu a, #avg90-nav-menu-dt-items a, #avg90-nav-menu-mob-dropdown a, #scroll-tb').click(function(e){
            var dest = 0;
            e.preventDefault();
            if ($(this.hash).offset().top > $(document).height()-$(window).height()){
                dest = $(document).height()-$(window).height();
            } else {
                dest = $(this.hash).offset().top;
            }
            $('html,body').animate({scrollTop:dest}, 600, 'swing');

            return false;
        });

        $('article a').removeAttr('target');
    });

})(jQuery, this);
