<?php get_header() ?>
<?php $title = get_the_title() ?>
<?php $url   = get_the_permalink() ?>

<section>
    <h1 class="author-archive-title"><?= ___( 'Author Archives for ').get_the_author() ?></h1>

    <?php if (get_the_author_meta('description')): ?>
        <?= get_avatar(get_the_author_meta('user_email')) ?>
        <h2 class="author-about"><?= ___( 'About ').get_the_author() ?></h2>
        <div class="author-about-wrapper"><?= wpautop(get_the_author_meta('description')) ?></div>
    <?php endif; ?>

    <?php while (have_posts()): the_post(); ?>
        <article id="post-<?= get_the_ID() ?>" <?php post_class('loop-item') ?>>
            <div class="cat-item-img">
                <?php if (has_post_thumbnail()): ?>
                    <a href="<?= $url ?>" title="<?= $title ?>"><?php the_post_thumbnail('medium_large') ?></a>
                <?php endif; ?>
            </div>

            <div class="cat-item-details">
                <h2>
                    <a href="<?= $url ?>" title="<?= $title ?>"><?= $title ?></a>
                </h2>
                <div class="cat-item-dateauthor">
                    <span class="cat-item-author"><?= get_the_author_posts_link() ?></span> |
                    <span class="cat-item-date"><?= get_the_time('d.m.Y') ?></span>
                </div>
            </div>
        </article>
    <?php endwhile; ?>

    <?php get_template_part('pagination') ?>
</section>

<?php get_footer() ?>
