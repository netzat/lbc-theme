<?php /**** Only in coral ****/

get_header(null, ['class' => 'post']);

$src = get_stylesheet_directory_uri().'/img/_coral/avg-ninety';
$url = urlencode(get_permalink(get_the_ID()));
$sm  = [
    'twitter'  => 'http://twitter.com/home?status='.urlencode(get_the_title()).' '.$url,
    'facebook' => 'http://www.facebook.com/sharer/sharer.php?u='.$url.'&title='.urlencode(get_the_title()),
    'gplus'    => 'https://plus.google.com/share?url='.$url
];

?>

<section>
    <article id="post-<?= get_the_ID() ?>" <?php post_class() ?>>
        <div class="page-body">
            <div class="page-content clear">
                <div id="avg90-home">

                    <div id="avg90-background">
                        <img id="avg90-mobile-bg" alt="" src="<?= $src ?>/mobile_bg.jpg" />
                        <img id="avg90-tablet-bg" alt="" src="<?= $src ?>/tablet_bg.jpg" />
                        <img id="avg90-desktop-bg" alt="" src="<?= $src ?>/desktop_bg.jpg" />
                    </div>

                    <div id="avg90-title">
                        <h1><b>Average 90 minutes</b>&nbsp;<span class="avg90-hide-desktop"><br /></span>Premier League Match</h1>
                        <a href="#intro" id="scroll-tb">KICK OFF</a>
                        <div><img src="<?= $src ?>/arrow_down.png" alt=""></div>
                    </div>

                    <div id="avg90-mins-menu" class="fixed">
                        <ul>
                            <li><a href="#content1">32 min</a></li>
                            <li><a href="#content2">47 min</a></li>
                            <li><a href="#content3">51 min</a></li>
                            <li><a href="#content4">52 min</a></li>
                            <li><a href="#content5">58 min</a></li>
                            <li><a href="#content6">59 min</a></li>
                            <li><a href="#content7">65 min</a></li>
                            <li><a href="#content8">69 min</a></li>
                            <li><a href="#content9">96 min</a></li>
                        </ul>
                    </div>

                    <div id="avg90-social-menu">
                        <ul>
                            <li><a class="twitter social-icon resize-bg" href="<?= $sm['twitter'] ?>" onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,' + 'width=600'); return false;"></a></li>
                            <li><a class="facebook social-icon resize-bg" href="<?= $sm['facebook'] ?>" onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,' + 'width=600'); return false;"></a></li>
                            <li><a class="g-plus social-icon resize-bg" href="<?= $sm['gplus'] ?>" onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,' + 'width=600');return false;"></a></li>
                            <li><a class="clipboard social-icon resize-bg" id="copy-to-clipboard" class="copy-to-clipboard resize-bg" href="#">.</a></li>
                        </ul>
                    </div>

                    <div id="avg90-nav-menu-mob">
                      <div class="dropbtn">Key moments</div>
                      <div id="avg90-nav-menu-mob-dropdown" class="dropdown-content">
                          <a href="#content1">First Goal</a>
                          <a href="#content8">Last Goal</a>
                          <a href="#content5">Yellow Card</a>
                          <a href="#content7"">Red Card</a>
                          <a href="#content4">Penalty Scored</a>
                      </div>
                    </div>
                </div>

                <div id="avg90-nav-menu-dt">
                    <div id="avg90-nav-menu-dt-items">
                      <a href="#content1">First Goal</a>
                      <a href="#content8">Last Goal</a>
                      <a href="#content5">Yellow Card</a>
                      <a href="#content7">Red Card</a>
                      <a href="#content4">Penalty Scored</a>
                    </div>
                </div>

                <div id="intro" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>Take a dive into 10 years of the English Premier League</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/1.intro_90_min.jpg" alt="Average 90 min - Intro" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Premier League football may be unpredictable, but there’s still a ton of interesting trends and patterns in England’s top flight games.</p>
                        <p>Over the past ten years there has been an exponential increase in the amount spent on transfer fees, player’s wages, training and managerial fees, but has this made a real difference to the average Premier League game? Statistically speaking the structure of the game has remained unchanged in many ways. In 10 years the average time the first goal is scored has reduced by only 60 seconds from the 33rd minute in 2006/7 down to the 32nd from 2014-17!</p>
                        <p>Join <b>Coral</b> on a tour of the average “90 minute” match with a breakdown of when the key events occur based on analysis of Opta data from 2014-2017.</p>
                    </div>
                </div>

                <div id="quote1" class="avg90-quote">
                    <p><i>"Essentially, what was yesterday’s competitive edge quickly becomes today’s normal, once other teams work it out and start also investing in that edge. You have to do so much more now just to maintain the status quo. This may mean that the game looks and feels different, as it has evolved, but the outcomes such as the time of key events stay similar."</i></p>
                    <p class="footer">Bradley Busch, Sports Psychologist</p>
                </div>

                <div id="content1" class="avg90-section">
                    <div class="avg90-section-title">
                         <h2>32 minutes: First Goal</h2>
                    </div>
                    <div class="avg90-section-content">
                        <p>As a rule, you can generally expect the first goal to come just after the half-hour mark. In each of the past three seasons, the average opening goal has come between the 32nd and 33rd minute.</p>
                        <p>This compares strongly with ten years ago, when the average time of the first goal in the 2006-07 season was the 33rd minute. So clearly some things never change.</p>
                        <p>The quickest ever Premier League goal came courtesy of Tottenham Hotspur’s Ledley King in December 2000, with the Spurs defender finding the net after just 9.7 seconds.</p>
                        <p>The fastest this season also came from Tottenham. Christian Eriksen netted after 11 seconds against Manchester United in January 2018. Leicester City’s Shinji Okazaki has also netted within the opening minute this season.</p>
                    </div>
                    <div class="avg90-section-img-container">
                         <img src="<?= $src ?>/2.first_goal.jpg" alt="Average 90 min - First Goal" />
                    </div>
                </div>

                <div class="avg90-inplay">
                    <div class="avg90-inplay-text">Visit our In-Play</div>
                    <div class="avg90-inplay-btn-container"><a href="http://sports.coral.co.uk/betinplay">BET NOW</a></div>
                </div>

                <div id="content2" class="avg90-section">
                    <div class="avg90-section-title">
                         <h2>47 minutes: Direct free-kick goals</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/3.direct_freekick_goals.jpg" alt="Average 90 min - Direct Free Kick Goals" />
                    </div>
                    <div class="avg90-section-content">
                        <p>When it comes to scoring from free-kicks Cristiano Ronaldo, Steven Gerrard, and Christian Eriksen might come to mind. But nobody in recent times can match the 11 Premier League free-kicks netted by Sebastian Larsson during his time with Birmingham City and Sunderland.</p>
                        <p>The average time for direct free-kick goals during the past three seasons has been the 47th minute. Whether it’s down to defenders failing to pick up the pace after half-time or sides surging forward after a rousing team talk, they’re generally coming just after the interval.</p>
                    </div>
                </div>

                <div id="fact1" class="avg90-fact">
                    <h2>Are you getting your money’s worth? </h2>
                    <p>Cheapest season ticket available to Arsenal fans cost £1,769 for the 2016/17 season. For these Arsenal season ticket holders, £687.94 of the ticket cost is spent watching no match gameplay. </p>
                    <p>Discover more with the <a href="https://news.coral.co.uk/football/premier-league/dead-ball-time">Coral guide to getting your money’s worth</a> from your season ticket!</p>
                </div>

                <div id="content3" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>51 minutes: Set-piece goals</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/4.setpieces_goals.jpg" alt="Average 90 min - Set Pieces Goals" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Last season’s <a href="https://sports.coral.co.uk/football/england/premier-league">Premier League winners</a> Chelsea topped the charts for goals from set-pieces, scoring 22 times from corners and free-kicks. However, it was West Bromwich Albion who had the biggest percentage of goals from dead-ball situations, with 48% of their goals coming from set-pieces.</p>
                        <p>Four players scored six goals or more from set-pieces last term. Two of them – Diego Costa, and Gary Cahill – were in Chelsea’s ranks, while veteran West Bromwich Albion defender Gareth McAuley and then-Swansea ace Fernando Llorente were the others.</p>
                        <p>The average time of set-piece goals across the Prem three seasons was around the 51-minute mark.</p>
                    </div>
                </div>

                <div id="content4" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>52 minutes: Penalty scored</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/5.penalty_scored.jpg" alt="Average 90 min - Penalty Scored" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Four sides shared the honour of netting the most penalties last term, with AFC Bournemouth, Liverpool, Manchester City and Tottenham Hotspur all scoring a magnificent seven. However, the personal accolades went to Harry Kane, who registered more times from the spot than any other player, with five.</p>
                        <p>The average time of penalties scored in 2016-17 was the 54th minute. In fact, over the last three seasons the average successful penalty has come on 53 minutes.</p>
                        <p>But there have been a string of them scored in the dying moments too.</p>
                        <p>Four of the 19 successful pens in the Prem this season have been scored in the 89th minute or later.</p>
                        <p>Head to sports.coral.co.uk/football for all the latest <a href="https://sports.coral.co.uk/football">football odds</a>.</p>
                    </div>
                </div>

                <div id="fact2" class="avg90-fact">
                    <h2>Did you know...</h2>
                    <p>Over the past three seasons, the ball has been in play an average of only 56:15 minutes. That’s 42% of each game spent watching no player action.</p>
                </div>

                <div id="content5" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>58 minutes: Yellow Card</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/6.yellow_card.jpg" alt="Average 90 min - Yellow Card" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Over the last three seasons, the average time for a yellow card has been remarkably consistent. It comes at a time when defenders tire and referees punish players for multiple fouls shortly after half-time.</p>
                        <p>In both 2014-15 and 2015-16 seasons, the average yellow card was shown in the 59th minute. And last season that trend occurred again, with referees getting out the yellow card in the 58th minute on average. </p>
                        <p>Watford finished 17th in the Premier League last year, but collecting yellow cards was one area where nobody came close to the Hornets. The Hertfordshire club racked up a total 84 bookings.</p>
                        <p>Meanwhile, Bournemouth were the best-behaved side on this front, with a modest 52 yellows.</p>
                    </div>
                </div>

                <div id="content6" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>59 minutes: First substitution</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/7.first_substitution.jpg" alt="Average 90 min - First substitution" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Whether your side are 1-0 down and chasing the game or trying to protect a lead, they’re likely to make the first change just before the hour mark. Over the past three seasons, managers have consistently waited almost 15 minutes after the interval, and the average time for the first sub has been the 59th minute. </p>
                        <p>The term ‘super sub’ is synonymous with players who’ve come off the bench to score vital goals.</p>
                        <p>The man with the most Premier League goals as a sub is Jermain Defoe. Over the course of his time at West Ham United, Tottenham, Portsmouth and Sunderland, he managed a combined 23 goals. Now with Bournemouth, he’ll be looking to protect that record with more goals from the bench.</p>
                    </div>
                </div>

                <div id="quote2" class="avg90-quote">
                    <p><i>"Over the last 10 years there have been huge investments in things like sport science, which has resulted in players being able to cover more ground etc.  But if, as is the case, all Premiership teams are investing then the gap between teams is likely to remain constant. "</i></p>
                    <p class="footer">Bradley Busch, Sports Psychologist</p>
                </div>

                <div id="content7" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>65 minutes: Red Card</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/8.red_card.jpg" alt="Average 90 min - Red Card" />
                    </div>
                    <div class="avg90-section-content">
                        <p>It’s generally well into the second half when the red mist descends for Premier League players, with the average sending off coming at the 65-minute mark.</p>
                        <p>Hull City, Watford and West Ham saw the most players sent for an early bath last term, with five reds apiece. At the other end of the scale, six sides including Liverpool, Chelsea and Swansea City avoided a single red.</p>
                    </div>
                </div>

                <div id="content8" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>69 minutes: Last Goal</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/9.last_goal.jpg" alt="Average 90 min - Last Goal" />
                    </div>
                    <div class="avg90-section-content">
                        <p>Everybody loves a late winner, but over the last three seasons Opta’s stats show that last goal occurs much earlier on average.</p>
                        <p>In 2014-15 and 2015-16, the average time of a game’s final goal was on 68 minutes, and last season that shifted only slightly to 70 minutes.</p>
                        <p>Manchester United have the best record beyond the 75-minute mark this season, scoring 12 goals and conceding none. The worst record belongs to Swansea City, who have scored none and shipped six in that period.</p>
                    </div>
                </div>

                <div id="content9" class="avg90-section">
                    <div class="avg90-section-title">
                        <h2>96 minutes: Average game length</h2>
                    </div>
                    <div class="avg90-section-img-container">
                        <img src="<?= $src ?>/10.average_game_ length.jpg" alt="Average 90 min - Average game length" />
                    </div>
                    <div class="avg90-section-content">
                        <p>They say it’s a 90-minute match, but that’s not strictly true. The average Premier League game in 2016-17 lasted 96:38 in total. That’s down to time taken up by injuries, substitutions and <a href="https://news.coral.co.uk/football/evolution-goal-celebrations/" target="_blank">overly-elaborate goal celebrations!</a></p>
                        <p>So while football is a game full of variables, the statistics and the data show that actually, most aspects of the game occur with near-scientific regularity and consistency. Looking back over the last three seasons and comparing to 10 years ago, we can only see a matter of seconds, or minutes between the average times the key events occur.</p>
                        <p>Check out our latest football odds on <a href="https://sports.coral.co.uk/football">sports.coral.co.uk/football</a>.</p>
                    </div>
                </div>

                <div id="avg90-footer">
                    <div id="back2top">
                        <img src="<?= $src ?>/arrow_up.png" alt="">
                        <a href="#avg90-home" id="scroll-tb">BACK TO TOP</a>
                    </div>
                    <div></div>
                    <div id="copyright"><i>Data powered by Opta.</i></div>
                </div>
            </div>
        </div>
    </article>
</section>

<?php get_footer() ?>
