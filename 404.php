<?php get_header(); ?>

<section>
    <article id="post-404">
        <h1><?= ___('Page not found') ?></h1>
        <h2><a href="<?= home_url() ?>"><?= ___( 'Return home?') ?></a></h2>
    </article>
</section>

<?php get_footer(); ?>
