<?php

get_header(null, ['class' => 'post']);

$getBreadcrumb = function(): string
{
    $sep   = '&nbsp;&nbsp;&#187;&nbsp;&nbsp;';
    $bc    = sprintf('<a href="%s" rel="nofollow">Home</a>%s', home_url(), $sep);
    $cat   = get_the_category();

    if (!$cat || $cat[0]->category_parent) {
        $bc .= get_the_category_list($sep);
    } else {
        $bc .= sprintf('<a href="%s">%s</a>', get_category_link($cat[0]), $cat[0]->name);
    }

    return $bc.$sep.get_the_title();
};

?>

<section>
    <article id="post-<?= get_the_ID() ?>" <?php post_class() ?>>

        <?php if (hasThumbnail()): ?>
            <?= get_the_post_thumbnail(isLadbrokes() ? 'large' : null) ?>
        <?php endif; ?>

        <div class="post-body">
            <div class="post-breadcrumb"><?= $getBreadcrumb() ?></div>
            <h1 class="post-title"><?= get_the_title() ?></h1>

            <div class="post-details">
                <?php if($author = get_the_author_posts_link()): ?>
                    <span class="author"><?= $author ?></span> |
                <?php endif; ?>
                <span class="date"><?= get_the_time('d.m.Y') ?></span>
            </div>

            <?php if (isCoral()): ?>
                <div class="post-btn post-main-btn">
                    <a href="<?= getConfig('top_button_link') ?>">
                        <span><?= getConfig('top_button_label') ?></span>
                    </a>
                </div>
            <?php endif; ?>

            <div class="post-content">
                <div class="post-desc"><?php the_content() ?></div>
                <?php if (isCoral()): ?>
                    <div class="post-banner">
                        <div class="side-top-banner"><img src="<?= img(getConfig('top_banner')) ?>"></div>
                        <div class="side-bottom-banner"><img src="<?= img(getConfig('bottom_banner')) ?>"></div>
                    </div>
                <?php endif; ?>
            </div>

            <div class="post-soc-med"><?= do_shortcode('[addtoany]') ?></div>
        </div>

    </article>

    <div class="post-navigate clear">
        <div class="post-navigate-left"><?= get_previous_post_link('&laquo; %link', '%title', true) ?></div>
        <div class="post-navigate-right"><?= get_next_post_link('%link &raquo;', '%title', true) ?></div>
    </div>

    <?php if (get_the_author()): ?>
        <div class="post-about-author">
            <p class="paa-title">Author</p>
            <p class="paa-name"><?= get_the_author() ?></p>
            <div class="paa-about"><?= wpautop(get_the_author_meta('description')) ?></div>
        </div>
    <?php endif; ?>

    <?php include('single-relatedposts.php'); ?>
</section>

<?php get_footer() ?>
